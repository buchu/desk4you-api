package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

func IndexHandler(w http.ResponseWriter, req *http.Request) {
	response := make(map[string]string)
	response["status"] = "OK"
	response["time"] = time.Now().String()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func DesksListHandler(w http.ResponseWriter, req *http.Request) {
	db := InitDb()
	defer db.Close()

	var desks []Desks
	db.Find(&desks)

	response := make(map[string]interface{})
	response["status"] = "OK"
	response["results"] = desks

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func DesksCreateHandler(w http.ResponseWriter, req *http.Request) {
	db := InitDb()
	defer db.Close()

	var desk Desks
	body, err := ioutil.ReadAll(io.LimitReader(req.Body, 1048576))
	checkErr(err)

	err = json.Unmarshal(body, &desk)
	db.Create(&desk)
	checkErr(err)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(desk)
}
