package main

import (
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	router := NewRouter()

	log.Fatal(http.ListenAndServe(":8080", router))
}

func InitDb() *gorm.DB {
	db, err := gorm.Open("sqlite3", "./desk4me.db")
	checkErr(err)

	db.LogMode(true)

	if !db.HasTable(&Desks{}) {
		db.CreateTable(&Desks{})
	}

	return db
}
