package main

type Desks struct {
	Id        int     `gorm:"AUTO_INCREMENT json:id"`
	Company   string  `gorm:"not null" json:"company"`
	Booked    bool    `gorm:"not null" json:"booked"`
	Latitude  float64 `gorm:"not null" json:"latitude"`
	Longitude float64 `gorm:"not null" json:"longitude"`
}
